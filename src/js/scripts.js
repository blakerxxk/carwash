(function ($, window, document, undefined) {

  'use strict';

  $(function () {

    if($('#datepicker').length > 0 && $('#datepicker').is(':visible')){
      $('#datepicker').datepicker();
    } else if($('#datepicker').length > 0 && !$('#datepicker').is(':visible')){
      $('.js-orders').on('click', function(){
        $('#datepicker').datepicker();
      });
    }



    $( '.video_placeholder--wrap' ).on({
      mouseenter: function() {
        $(this).find('.overlay--white').show();
      },
      mouseleave: function() {
        $(this).find('.overlay--white').hide();
      },
    });
  });

})(jQuery, window, document);
